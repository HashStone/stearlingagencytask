function getRandomInt(min, max)
{
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
var firstRandom; 
var secondRandom;
var pointsVal = +document.getElementById('points').textContent;
var points = document.getElementById('points');
var result;
var sec = 60;
var choices;
var btn1 = document.getElementById('btn-1');
var btn2 = document.getElementById('btn-2');
var btn3 = document.getElementById('btn-3');
var btn4 = document.getElementById('btn-4');
var firstField = document.getElementById("num1");
var secondField = document.getElementById("num2");
function recalc() {
    sec = 60;
    firstRandom = getRandomInt(1, 100);
    secondRandom = getRandomInt(1, 100);
    firstField.innerHTML = firstRandom;
    secondField.innerHTML = secondRandom;
    result = firstRandom * secondRandom;
    console.log("правильный ответ: " + result);
    choices = [
        result,
        getRandomInt(result-50, result+50),
        getRandomInt(result-50, result+50),
        getRandomInt(result-50, result+50)
    ];
    choices.sort();
    btn1.value=choices[0];
    btn2.value=choices[1];
    btn3.value=choices[2];
    btn4.value=choices[3];
}

function check(input) {
    if(firstRandom * secondRandom == input.value){
        pointsVal += 100;
        points.textContent = pointsVal;
        recalc();
    } else {
        sec=60;
        pointsVal -= 100;
        points.textContent = pointsVal;
        recalc();
        if (pointsVal < 0) {
            alert("вы проиграли, страница будет перезагружена");
            location.reload();
        }
    }
}
function refresh()
{
	sec--;
	if(sec<=9){sec="0" + sec;}
	time=sec;
	if(document.getElementById){timer.innerHTML=time;}
	inter=setTimeout("refresh()", 1000);
	if(sec=='00'){
		sec="00";
		clearInterval(inter);
        alert("время вышло. Нажмите ОК что бы начать заново");
        location.reload();
	}
}
